const config = require('./config');
const base = require('./base');

const {
  OUTPUT,
  DEVTOOL
} = config;

module.exports = {
  output: {
    path: OUTPUT.dir,
    filename: OUTPUT.file,
    publicPath: OUTPUT.public
  },
  resolve: {
    extensions: base.resolve.extensions,
    alias: base.alias
  },
  node: base.node,
  devtool: DEVTOOL
};
