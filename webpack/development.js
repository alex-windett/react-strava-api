const webpack = require('webpack');
const path = require('path');
const config = require('./config');
const base = require('./base');
const common = require('./common');

const {
  ENTRYPATH,
  ENTRY
} = config;

const {
  output,
  resolve,
  node,
  devtool
} = common;

module.exports = {
  entry: [
    'webpack-dev-server/client?http://localhost:5000',
    'webpack/hot/dev-server',
    ENTRY
  ],
  output,
  resolve,
  node,
  devtool,
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  module: {
    rules: [{
      test: /\.js?$/,
      use: [
        'babel-loader'
      ],
      include: ENTRYPATH
    }, {
      test: /\.css$/,
      use: [
        'style-loader?sourceMap',
        // eslint-disable-next-line
        'css-loader?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]'
      ]
    }]
  }
};

