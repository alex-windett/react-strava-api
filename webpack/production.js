const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const config = require('./config');
const base = require('./base');
const common = require('./common');

const {
  ENTRYPATH,
  ENTRY
} = config;

const {
  output,
  resolve,
  node,
  devtool
} = common;

module.exports = {
  entry: ENTRY,
  output,
  resolve,
  node,
  devtool,
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new ExtractTextPlugin('app.css')
  ],
  module: {
    rules: [{
      test: /\.js?$/,
      use: ['babel-loader'],
      include: ENTRYPATH
    }, {
      test: /\.css$/,
      use: ExtractTextPlugin.extract('style-loader', 'css-loader')
    }]
  }
};
