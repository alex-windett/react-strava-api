var path = require('path');

var env = process.env.NODE_ENV;
var isProduction = () => env === 'production';

module.exports = {
  ENTRY: path.join(__dirname, '../src/index.js'),
  ENTRYPATH: path.join(__dirname, '../src/'),
  OUTPUT: {
    dir:  path.join(__dirname, '../dist'),
    file: 'bundle.js',
    public: !isProduction() ? '/static/' : '/'
  },
  DEVTOOL: !isProduction() ? 'eval-source-map' : 'source-map'
};
