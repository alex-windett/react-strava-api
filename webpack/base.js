const path = require('path');
const fs = require('fs');

const config = require('./config');

const {
  ENTRYPATH
} = config;

module.exports = {
  resolve: {
    extensions: ['.js', '.json', '.jst', '.css']
  },
  alias: {
    '@components': path.join(ENTRYPATH, 'components'),
    '@containers': path.join(ENTRYPATH, 'containers'),
    '@shared': path.join(ENTRYPATH, 'shared'),
    '@store': path.join(ENTRYPATH, 'store'),
    '@helpers': path.join(ENTRYPATH, 'helpers')
  }
};
