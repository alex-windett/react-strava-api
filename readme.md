# Strava React App

This app will utilize the strava v3 api by using axios to communicate with it

Currently set to use my details by default, in future I intend to use oauth and 
open it up widley

## Use 
`yarn install`
`npm start`

open [localhost:5000](localhost:5000) in your browser

#### Roadmap

- Allow for segment comparison to other athletes
- Impliment testing and add to CI pipelines