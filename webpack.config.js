const webpack = require('webpack');
const clear = require('clear');

const env = process.env.NODE_ENV;
const configEnv = env !== 'prodcution' ? 'development' : 'production';
const webpackConfig = require(`./webpack/${configEnv}`);

clear();

console.log('***********************************');
console.log('Webpack building in', configEnv);
console.log('***********************************');

module.exports = webpackConfig;
