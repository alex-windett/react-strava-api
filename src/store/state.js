const stravaRoute = (route = '') => `/strava/${route}`;

const state = {
  'athlete': null,
  'ui': {
    'header': {
      'links': {
        'home': '/',
        'stava': stravaRoute(),
        'totals': stravaRoute('totals'),
        'segments': stravaRoute('segments'),
        'activties': stravaRoute('activities'),
        'login': '/log-in'
      }
    },
    'footer': {
      'links': {
        'email': 'someLinkHere',
        'twitter': 'someLinkHere',
        'cv': 'someLinkHere',
        'strava': 'someLinkHere'
      }
    }
  }
};

export default state;
