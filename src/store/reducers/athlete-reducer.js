import {
  handleActions
} from 'redux-actions';

import {
  saveAthleteToStore,
  saveTotalsToStore,
  saveSegmentsToStore,
  saveActivitiesToStore
} from '../actions';

export default handleActions({
  [saveAthleteToStore.toString()]: (state, action) => {
    return {
      ...state,
      profile: {
        ...action.payload
      }
    };
  },
  [saveSegmentsToStore.toString()]: (state, action) => {
    return {
      ...state,
      starredSegments: [
        ...action.payload
      ]
    };
  },
  [saveActivitiesToStore.toString()]: (state, action) => {

    return {
      ...state,
      activities: [
        ...action.payload
      ]
    }
  },
  [saveTotalsToStore.toString()]: (state, action) => {

    const {
      allRideTotals,
      recentRideTotals,
      biggestClimbElevationGain,
      biggestRideDistance,
      ytdRideTotals,
      allRunTotals,
      recentRunTotals,
      ytdRunTotals
    } = action.payload;

    return {
      ...state,
      totals: {
        ride: {
          all: allRideTotals,
          recent: recentRideTotals,
          biggestClimb: biggestClimbElevationGain,
          biggestDistance: biggestRideDistance,
          ytd: ytdRideTotals
        },
        run: {
          all: allRunTotals,
          recent: recentRunTotals,
          ytd: ytdRunTotals
        }
      }
    };
  }
}, {});
