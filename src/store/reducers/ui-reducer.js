import {
  handleActions
} from 'redux-actions';

import {
  toggleNav
} from '@store/actions';

export default handleActions({
  [toggleNav.toString()]: (state, actions) => {}
}, {});
