import {
  combineReducers 
} from 'redux';

import athleteReducer from './athlete-reducer';
import uiReducer from './ui-reducer';

const reducers = combineReducers({
  athlete: athleteReducer,
  ui: uiReducer
});

export default reducers;
