import thunk from 'redux-thunk';
import {
  createStore,
  compose,
  applyMiddleware
} from 'redux';

import state from './state';
import reducers from './reducers';

const _state = state;
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
    reducers,
    _state,
    composeEnhancers(
      applyMiddleware(
        thunk
      )
    )
  );
