import { createAction } from 'redux-actions';

const APP = 'WINDETT';

/****************** STRAVA ******************/
export const saveAthleteToStore =
  createAction(`${APP}/SAVE_ATHLETE_TO_STORE`);
export const saveTotalsToStore =
  createAction(`${APP}/SAVE_ATHLETE_TOTALS_TO_STORE`);
export const saveSegmentsToStore =
  createAction(`${APP}/SAVE_ATHLETE_SEGMENTS_TO_STORE`);
export const saveActivitiesToStore =
  createAction(`${APP}/SAVE_ATHLETE_ACTIVITIES_TO_STORE`);

export const toggleNav = createAction(`${APP}/TOGGLE_UI_NAV`);
