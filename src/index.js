import React from 'react';
import { render } from 'react-dom';
import { Grid } from 'react-flexbox-grid';
import Root from '@containers';

render((
  <Grid>
    <Root />
  </Grid>
), document.getElementById('root'));
