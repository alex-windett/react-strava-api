import React from 'react';
import PropTypes from 'prop-types';

const Empty = ({
  children,
  title
}) => {

  return (
    <div>
      <h1>{title} page</h1>
      {children}
    </div>
  );
};

Empty.PropTypes = {
  children: PropTypes.node,
  title: PropTypes.string
};

export default Empty;
