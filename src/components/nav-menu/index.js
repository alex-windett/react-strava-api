import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const Nav = ({
  ui
}) => {

  const {
    header,
    footer
  } = ui;

  const getLinks = (obj) => Object.keys(obj).map( (l, i) => (
    <li key={i}>
      <Link to={obj[l]}>{l}</Link>
    </li>
  ));

  return (
    <div>
      <div>
        <header>
          <ul>
            {getLinks(header.links)}
          </ul>
        </header>

        <footer>
          <ul>
            {getLinks(footer.links)}
          </ul>
        </footer>
      </div>
    </div>
  );
};

Nav.PropTypes = {
  ui: PropTypes.object
};

export default Nav;

