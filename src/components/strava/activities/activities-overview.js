import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';

const formatDate = (d) => moment(new Date(d)).format('Do MM YYYY');

const ActivitiesOverview = ({
  name = '',
  activities = []
}) => {

  let heartRateItems = [];
  let maxHeartActivity = {}

  activities.forEach( activity => {
    if ( activity.has_heartrate ) {
      heartRateItems.push(activity)
    }
  });

  heartRateItems.forEach( currentItem => {
    if ( !maxHeartActivity.max_heartrate ) {
      maxHeartActivity = currentItem
    } else if ( currentItem.max_heartrate >= maxHeartActivity.max_heartrate ) {
      maxHeartActivity = currentItem;
    }
  })

  const activitiesList = activities.map( activity => (
    <li key={activity.id}>
      <Link to={`/activity/${activity.id}`}>{activity.name} - {formatDate(activity.start_date)}</Link>
    </li>
  ));

  const HeartRateText = () => (
    <div>
      <p>Your max heart rate recently is <strong>{maxHeartActivity.max_heartrate} BPM</strong></p>
      <p> From: <Link to={`/activity/${maxHeartActivity.id}`}>{maxHeartActivity.name}</Link> on {formatDate(maxHeartActivity.start_date)}</p>
    </div>
  );

  return (
    <div>
      { maxHeartActivity.max_heartrate ? HeartRateText() : null }
      <ul>
        {activitiesList}
      </ul>
    </div>
  );
};

export default ActivitiesOverview;