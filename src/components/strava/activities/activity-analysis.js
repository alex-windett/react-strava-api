import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';

const formatDate = (d) => moment(new Date(d)).format('Do MM YYYY');

const ActivityAnalysis = ({
   activity
}) => {

  return (
    <div>
      <h1>{activity.name} - {activity.distance}</h1>
      <h3>{activity.start_date}</h3>
      <p><a href={`https://www.strava.com/activities/${activity.id}`} target="_blank">Strava Link</a></p>


      {JSON.stringify(activity)}
    </div>
  );
};

export default ActivityAnalysis;