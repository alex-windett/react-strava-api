import React from 'react';
import PropTypes from 'prop-types';
import { LineChart } from 'react-d3';
import data from '../../data/segment';

import { units } from '@helpers';

const SegmentsComponent = ({
  children,
  segment = {}
}) => {
//console.log(segment)
  const chartSeries = [{
    field: 'distance',
    name: 'Distance',
    color: '#000'
  }];

  const viewBoxObject = {
    x: 0,
    y: 0,
    width: 500,
    height: 400
  };

  return (
    <div>
      <p>There have been {segment.entry_count} attempts at this segment.</p>
      {children}

      {JSON.stringify(data)}

      <LineChart
        title='Segment Comparison'
        legend
        data={data}
        width={700}
        height={600}
        yAxisLabel='Height'
        xAxisLabel='Distance'
        gridHorizontal
        chartSeries={chartSeries}
        viewBoxObject={viewBoxObject}
      />
    </div>
  );
};

SegmentsComponent.PropTypes = {
  children: PropTypes.node,
  segment: PropTypes.object
};

export default SegmentsComponent;
