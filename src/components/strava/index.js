import React from 'react';
import PropTypes from 'prop-types';

const Index = ({
 children
}) => {

  return (
    <div>
      <h1>Strava Index page</h1>
      {children}
    </div>
  );
};

Index.PropTypes = {
  children: PropTypes.node
};

export default Index;
