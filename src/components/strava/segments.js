import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

import { units } from '@helpers';

const SegmentsComponent = ({
  athlete,
  children,
  segments = []
}) => {

  const _segments = segments.map( (segment, i) => (
    <div key={i}>
      <h2><Link to={`/strava/segment/${segment.id}`}>{segment.name}</Link></h2>
      <ul>
        <li>PR time: {segment.athlete_pr_effort.elapsed_time}</li>
        <li>PR date: {segment.athlete_pr_effort.start_date}</li>
      </ul>
    </div>
  ));

  return (
    <div>
      <h1>Stared Segments for {athlete.profile.firstname}</h1>

      {_segments}

      {children}
    </div>
  );
};

SegmentsComponent.PropTypes = {
  athlete: PropTypes.object,
  children: PropTypes.node,
  segments: PropTypes.object
};

export default SegmentsComponent;
