import React from 'react';
import PropTypes from 'prop-types';

import { units } from '@helpers';

const TotalsComponent = ({
 children,
 athlete
}) => {

  const {
    friendlyDistance
  } = units;

  const totalDistance = {
    km: friendlyDistance(athlete.totals.ride.all.distance).pretty,
    m: friendlyDistance(athlete.totals.ride.all.distance).metersPretty
  };

  return (
    <div>
      <h1>Totals page for {athlete.profile.firstname}</h1>

      <p>Total cycling distance: </p>
      <ul>
        <li>{totalDistance.km}</li>
        <li>{totalDistance.m}</li>
      </ul>
      {children}
    </div>
  );
};

TotalsComponent.PropTypes = {
  athlete: PropTypes.object,
  children: PropTypes.node
};

export default TotalsComponent;
