import { random, round } from 'mathjs';

let d = 0;
let h = 0;

const rndDistance = (
    reset = false
) => {

  if ( reset ) {
    d = h = 0;
  }

  d = round(random(d, d+10));
  h = round(random(h, h+10));
  return {
    d,
    h
  };
};

const data = [
  {
    name: 'series1',
    values: [{
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    },{
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    },{
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    },{
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }],
    strokeWidth: 3,
    strokeDashArray: '5,5'
  }, {
    name: 'series2',
    values:  [{
      x: rndDistance(true).d,
      y: rndDistance(true).h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    },{
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    },{
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    },{
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }, {
      x: rndDistance().d,
      y: rndDistance().h
    }]
  }
];

export default data;
