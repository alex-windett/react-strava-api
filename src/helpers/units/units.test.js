import { friendlyDistance } from './';

test('Unit calculation', () => {
  const distance = friendlyDistance(1000);

  expect(distance).toBeTruthy();
  expect(distance.pretty).toBe('1,000km');
  expect(distance.meters).toBe('1000000');
  expect(distance.metersPretty).toBe('1,000,000m');
});
