const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const friendlyDistance = (distance, units) => {

  // TODO: componsate for miles input
  return {
    pretty: `${numberWithCommas(distance)}km`,
    meters: `${distance / 0.001}`,
    metersPretty: `${numberWithCommas(distance / 0.001)}m`
  };
};

const units = {
  friendlyDistance
};

export default units;
