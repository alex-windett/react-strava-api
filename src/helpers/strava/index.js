import axios from 'axios';

const baseUrl = 'https://www.strava.com/api/v3/';

export const getEndpoint = (
  endpoint,
  success,
  err,
  args = {
    accessTkn:
      process.env.ACCESS_TOKEN
      || '9277e23593172a9935c6bb6891141d2695addf37'
  }
) => {

  if ( !args.accessTkn ) {
    console.warn('There is no access token');
    return;
  }

  const url = baseUrl + endpoint;

  axios.get(url, {
    params: {
      'access_token': args.accessTkn
    }
  })
    .then( (response) => {
      if ( typeof success !== 'function' ) {
        return;
      }

      success(response.data);
    })
    .catch( (error) => {
      if ( typeof err !== 'function' ) {
        console.warn(err, 'is not a function');
        return;
      }

      err(error);
    });
};

const strava = {
  getEndpoint
};

export default strava;
