import React from 'react';
import { Provider } from 'react-redux';

import store from '@store';
import { default as AppRouter } from './router';

const Root = () => {
  return (
    <Provider store={store}>
      <AppRouter />
    </Provider>
  );
};

export default Root;
