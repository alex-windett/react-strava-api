import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col } from 'react-flexbox-grid';

import {
  strava,
  units
} from '@helpers';

import { saveActivitiesToStore } from '@store/actions';
import { ActivitiesOverview } from '@components/strava/activities'

class Activities extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activityCount: 50
    }
  }

  componentWillMount() {

    const {
      getEndpoint
    } = strava;

    const {
      athlete,
      saveActivities,
      activities
    } = this.props;

    const {
      activityCount
    } = this.state;

    if ( !activities ) {
      getEndpoint(`athlete/activities?per_page=${activityCount}`,
        (d) => saveActivities(d),
        (e) => console.log(e)
      );
    }
  };

  filterActivities(activities = []) {

    let sortedActivities = {};

    activities.map( (activity, i) => {

      if ( !sortedActivities[activity.type] ) {
        sortedActivities[activity.type] = []
      };

      sortedActivities[activity.type][i] = activity;
    });

    return sortedActivities;
  }

  render() {

    const {
      activities
    } = this.props;

    const {
      activityCount
    } = this.state;

    if ( !activities ) {
      return <p>Pending....</p>
    };

    const {
      Ride,
      Run
    } = this.filterActivities(activities);

    return (
      <div>
        <Row>
          <Col sm={12}>
            Takes you latest {activityCount} activities and does stuff
          </Col>
        </Row>
        <Row>
          <Col sm={6}>
            <div>
              <ActivitiesOverview activities={Ride} name='ride' />
            </div>
          </Col>
          <Col sm={6}>
            <ActivitiesOverview activities={Run} name='run' />
          </Col>
        </Row>
      </div>
    );
  }
}

Activities.PropTypes  = {
  athlete: PropTypes.object,
  saveSegments: PropTypes.func,
  activities: PropTypes.object
};

const mapStateToProps = (store) => ({
  athlete: store.athlete,
  segments: store.athlete.starredSegments,
  activities: store.athlete.activities
});

const mapDispatchToProps = (dispatch) => ({
  saveActivities: (d) => dispatch(saveActivitiesToStore(d))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Activities);
