import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import  SegmentsComponent from '@components/strava/segments';

import {
  strava,
  units
} from '@helpers';

import { saveSegmentsToStore } from '@store/actions';

class Segments extends React.Component {

  componentWillMount() {

    const {
      getEndpoint
    } = strava;

    const {
      athlete,
      saveSegments,
      segments
    } = this.props;

    if ( !segments ) {
      getEndpoint(`athletes/${athlete.profile.id}/segments/starred`,
        (d) => saveSegments(d),
        (e) => console.log(e)
      );
    }
  }

  render() {

    const {
      athlete,
      segments
    } = this.props;

    if ( !athlete.starredSegments ) {
      return <h1>Loading....</h1>;
    }

    return (
      <SegmentsComponent
        athlete={athlete}
        segments={segments}
      />
    );
  }
}

Segments.PropTypes  = {
  athlete: PropTypes.object,
  saveSegments: PropTypes.func,
  segments: PropTypes.object
};

const mapStateToProps = (store) => ({
  athlete: store.athlete,
  segments: store.athlete.starredSegments
});

const mapDispatchToProps = (dispatch) => ({
  saveSegments: (d) => dispatch(saveSegmentsToStore(d))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Segments);
