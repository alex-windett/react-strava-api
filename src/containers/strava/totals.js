import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TotalsComponent from '@components/strava/totals';

import {
  strava,
  units
} from '@helpers';

import { saveTotalsToStore } from '@store/actions';

class Totals extends React.Component {

  componentWillMount() {

    const {
      getEndpoint
    } = strava;

    const {
      athlete,
      saveTotals,
      totals
    } = this.props;

    if ( !totals ) {
      getEndpoint(`athletes/${athlete.profile.id}/stats`,
        (d) => saveTotals(d),
        (e) => console.log(e)
      );
    }
  }

  render() {

    const {
      athlete
    } = this.props;

    if ( !athlete.totals ) {
      return <h1>Loading....</h1>;
    }

    return <TotalsComponent athlete={athlete} />;
  }
}

Totals.PropTypes  = {
  athlete: PropTypes.object,
  saveTotals: PropTypes.func,
  totals: PropTypes.object
};

const mapStateToProps = (store) => ({
  athlete: store.athlete,
  totals: store.athlete.totals
});

const mapDispatchToProps = (dispatch) => ({
  saveTotals: (d) => dispatch(saveTotalsToStore(d))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Totals);
