import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import  SegmentComponent from '@components/strava/segment';

import {
  strava,
  units
} from '@helpers';

class Segment extends React.Component {

  componentWillMount() {

    this.setState({
      segment: null
    });

    const {
      getEndpoint
    } = strava;

    const {
      params
    } = this.props;

    getEndpoint(`segments/${params.id}/leaderboard`,
      (d) => this.saveSegment(d),
      (e) => console.log(e)
    );
  }

  componentWillUnmount() {
    this.setState({
      segment: null
    });
  }

  saveSegment(d) {
    this.setState({
      segment: d
    });
  }

  render() {
    const {
      profile
    } = this.props;

    const {
      segment
    } = this.state;

    if ( !segment ) {
      return <h1>Nothing here</h1>;
    }

    return (
      <SegmentComponent
        athlete={profile}
        segment={segment}
      />
    );
  }
};

Segment.PropTypes = {
  profile: PropTypes.object,
  params: PropTypes.object
};

const mapStateToProps = (store) => ({
  athlete: store.athlete
});

export default connect(
  mapStateToProps
)(Segment);
