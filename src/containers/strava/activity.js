import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';

import {
  strava,
  units
} from '@helpers';

import { saveActivitiesToStore } from '@store/actions';
import { ActivityAnalysis } from '@components/strava/activities'

const formatDate = (d) => moment(new Date(d)).format('Do MM YYYY');

class Activity extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      activity: {}
    }
  }

  componentWillMount() {
    const {
      getEndpoint
    } = strava;

    const {
      params
    } = this.props;

    getEndpoint(`activities/${params.id}`,
      (activity) => this.setState({ activity }),
      (e) => console.log(e)
    );
  }

  render() {

    const {
      activity
    } = this.state;

    if ( !activity ) {
      return (
        <h2>Loading...</h2>
      )
    };

    return (
      <div>
        <ActivityAnalysis activity={activity} />
      </div>
    );
  }
};

export default Activity;