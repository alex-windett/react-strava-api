import React from 'react';
import { connect } from 'react-redux';

import Index from '@components/strava';

const Post = (props) => (
  <Index {...props} />
);

const mapStateToProps = (store) => {
  return {
    athlete: store.athlete
  };
};

export default connect(mapStateToProps)(Post);
