import {
  Router,
  Route,
  browserHistory,
  IndexRoute
} from 'react-router';
import React from 'react';

import Home from './home';
import NoMatch from './no-match';
import Main from './main';
import LogIn from './log-in';

import Strava from './strava';
import Totals from './strava/totals';
import Segments from './strava/segments';
import Activities from './strava/activities';
import Activity from './strava/activity';
import Segment from './strava/segment';

/* ######################################## */
/* ################ ROUTES ################ */
/* ######################################## */
// TODO: Remvove "strava" from paths
const strava          = '/strava';
const stravaTotals    = `${strava}/totals`;
const stravaSegments  = `${strava}/segments`;
const stravaSegment   = `${strava}/segment/:id`;
const stravaActivities= `${strava}/activities`;
const stravaActivity  = `activity/:id`;

export default () => (
  <Router history={browserHistory}>
    <Route path='/' component={Main}>
      <IndexRoute component={Home} />
      <Route path='/log-in' component={LogIn} />
      <Route path={strava} component={Strava} />
      <Route path={stravaTotals} component={Totals} />
      <Route path={stravaSegments} component={Segments} />
      <Route path={stravaSegment} component={Segment} />
      <Route path={stravaActivities} component={Activities} />
      <Route path={stravaActivity} component={Activity} />
      <Route path='*' component={NoMatch} />
    </Route>
  </Router>
);
