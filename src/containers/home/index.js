import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class Home extends React.Component {

  render() {

    const {
      athlete
    } = this.props;

    const fullName = athlete.profile.firstname + athlete.profile.lastname;

    return (
      <div>
        <h1>Welcome to your new Home Page</h1>
        <h2>{fullName}</h2>
        <img src={athlete.profile.profile} alt={fullName} />
      </div>
    );
  }
}


Home.PropTypes  = {
  athlete: PropTypes.object
};

const mapStateToProps = (store) => ({
  athlete: store.athlete
});

export default connect(
  mapStateToProps,
)(Home);
