import React from 'react';
import { Row, Col } from 'react-flexbox-grid';
import classNames from 'classnames';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import styles from './styles.css';
import Nav from '@containers/nav';
import { strava } from '@helpers';
import { saveAthleteToStore } from '@store/actions';

class Main extends React.Component {

  componentDidMount() {

    const {
      getEndpoint
    } = strava;

    const {
      saveAthlete,
      athlete
    } = this.props;

    if ( !athlete ) {
      getEndpoint('athlete', (d) => saveAthlete(d));
    }
  }

  render() {
    const {
      athlete,
      children
    } = this.props;

    const cn = classNames(
      styles.main,
      'wrapper'
    );

    if ( !athlete ) {
      return (
        <h1>Loading....</h1>
      );
    }

    return (
      <div>
        <Nav />

        <Row className={cn}>
          <Col xs={12}>
            {children}
          </Col>
        </Row>
      </div>
    );
  }
};

Main.PropTypes = {
  athlete: PropTypes.object,
  children: PropTypes.node,
  saveAthlete: PropTypes.func
};

const mapStateToProps = (store) => ({
  athlete: store.athlete
});

const mapDispatchToProps = (dispatch) => ({
  saveAthlete: (d) => dispatch(saveAthleteToStore(d))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
