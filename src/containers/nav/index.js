;import React from 'react';
import { connect } from 'react-redux';
import NavMenu from '@components/nav-menu';

import {
  toggleNav
} from '@store/actions';

const Nav = (props) => (
  <NavMenu {...props} />
);

const mapStateToProps = (store) => ({
  posts: store.posts,
  ui: store.ui
});

const mapDispathToProps = (dispatch) => ({
  toggleSideNav: () => dispatch(toggleNav)
});

export default connect(
  mapStateToProps,
  mapDispathToProps
)(Nav);
